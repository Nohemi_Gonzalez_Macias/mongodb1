# MongoDB1: Tarea_1
Consultas de calificaciones de estudiantes en MongoDB.
Elaborado por: Nohemí González Macías, 301403

# 1) Mongoimport
  Baja el archivo grades.json y en la terminal ejecuta el comando `$mongoimport -d students -c grades < grades.json`

![Comando mongoimport](tarea_1_screens/1.png)

# 2) Comprobar que se hizo correctamente la importación 
  El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo:

![Comprobación de la importación de la colección](tarea_1_screens/2.png)

# 3) Encontrar calificaciones de un id específico 
  Encuentra todas las calificaciones del estudiante con el id numero 4

![Calificaciones id 4](tarea_1_screens/3.png)

# 4) Tipos de registros #1
  ¿Cuántos registros hay de tipo exam?

![Registros exam](tarea_1_screens/4.png)

# 5) Tipos de registros #2
  ¿Cuántos registros hay de tipo homework?

![Registros homework](tarea_1_screens/5.png)

# 6) Tipos de registros #3
  ¿Cuántos registros hay de tipo quiz?

![Registros quiz](tarea_1_screens/6.png)

# 7) Eliminación de registros con un id específico
  Elimina todas las calificaciones del estudiante con el id numero 3

![Eliminar id 3](tarea_1_screens/7.png)

# 8) Consulta de una calificacion específica
  ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea?

![Consulta calificación específica](tarea_1_screens/8.png)

# 9) Actualizar calificaciones de un UUID
  Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100

![Actualizar calificaciones de un UUID](tarea_1_screens/9.png)

# 10) Consulta de calificación
  ¿A qué estudiante le pertenece esta calificación?

![Consulta calificación](tarea_1_screens/10.png)
