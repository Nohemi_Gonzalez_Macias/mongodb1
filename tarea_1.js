/* Tarea 1: MongoDB1
   301403: Nohemí González Macías */

// 1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando:

    // Comandos:
      $ mongoimport -d students -c grades < grades.json
    /* Resultados:
      ➜  tarea_1 git:(master) ✗ mongoimport -d students -c grades < grades.json
      2018-10-14T00:48:27.192-0600	connected to: localhost
      2018-10-14T00:48:27.391-0600	imported 800 documents
      ➜  tarea_1 git:(master) ✗ */

// 2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo:

    // Comandos:
       use students;
       db.grades.count();
    /* Resultados:
       use students;
       switched to db students
       db.grades.count();
       800

   ¿Cuántos registros arrojo el comando count?
    El comando arrojó 800 registros. */

// 3) Encuentra todas las calificaciones del estudiante con el id numero 4

    // Comandos:
      db.grades.find({student_id: 4}).pretty();
    /* Resultados:
      {
      	"_id" : ObjectId("50906d7fa3c412bb040eb587"),
      	"student_id" : 4,
      	"type" : "exam",
      	"score" : 87.89071881934647
      }
      {
      	"_id" : ObjectId("50906d7fa3c412bb040eb588"),
      	"student_id" : 4,
      	"type" : "quiz",
      	"score" : 27.29006335059361
      }
      {
      	"_id" : ObjectId("50906d7fa3c412bb040eb589"),
      	"student_id" : 4,
      	"type" : "homework",
      	"score" : 5.244452510818443
      }
      {
      	"_id" : ObjectId("50906d7fa3c412bb040eb58a"),
      	"student_id" : 4,
      	"type" : "homework",
      	"score" : 28.656451042441
      }*/

// 4) ¿Cuántos registros hay de tipo exam?

    // Comandos:
       db.grades.find({type:"exam"}).count();
    /* Resultados:
       200 */

// 5) ¿Cuántos registros hay de tipo homework?

   // Comandos:
      db.grades.find({type:"homework"}).count();
   /* Resultados:
      400 */

// 6) ¿Cuántos registros hay de tipo quiz?

   // Comandos:
      db.grades.find({type:"quiz"}).count();
   /* Resultados:
      200 */

// 7) Elimina todas las calificaciones del estudiante con el id numero 3

   // Comandos:
      db.grades.find({student_id: 3}).pretty();
      db.grades.deleteMany({student_id: 3});
   /* Resultados:
     {
      	"_id" : ObjectId("50906d7fa3c412bb040eb584"),
      	"student_id" : 3,
      	"type" : "quiz",
      	"score" : 82.59760859306996
      }
      {
      	"_id" : ObjectId("50906d7fa3c412bb040eb585"),
      	"student_id" : 3,
      	"type" : "homework",
      	"score" : 50.81577033538815
      }
      {
      	"_id" : ObjectId("50906d7fa3c412bb040eb583"),
      	"student_id" : 3,
      	"type" : "exam",
      	"score" : 92.6244233936537
      }
      {
      	"_id" : ObjectId("50906d7fa3c412bb040eb586"),
      	"student_id" : 3,
      	"type" : "homework",
      	"score" : 92.71871597581605
      }
      { "acknowledged" : true, "deletedCount" : 4 } */

// 8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea?

   // Comandos:
      db.grades.find({score: 75.29561445722392}).pretty();
   /* Resultados:
   {
      "_id" : ObjectId("50906d7fa3c412bb040eb59e"),
      "student_id" : 9,
      "type" : "homework",
      "score" : 75.29561445722392
    }*/

// 9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100

   // Comandos:
      db.grades.find({"_id" : ObjectId("50906d7fa3c412bb040eb591")}).pretty();
      db.grades.updateMany({"_id" : ObjectId("50906d7fa3c412bb040eb591")}, {$set:{score:100}});
      db.grades.find({"_id" : ObjectId("50906d7fa3c412bb040eb591")}).pretty();
  /* Resultados:
    {
    	"_id" : ObjectId("50906d7fa3c412bb040eb591"),
    	"student_id" : 6,
    	"type" : "homework",
    	"score" : 81.23822046161325
    }
    { "acknowledged" : true, "matchedCount" : 1, "modifiedCount" : 1 }
    {
    	"_id" : ObjectId("50906d7fa3c412bb040eb591"),
    	"student_id" : 6,
    	"type" : "homework",
    	"score" : 100
    }*/

// 10) ¿A qué estudiante pertenece esta calificación?

   // Comandos:
      db.grades.find({score:100}).pretty();
   /* Resultados:
     {
      "_id" : ObjectId("50906d7fa3c412bb040eb591"),
      "student_id" : 6,
      "type" : "homework",
      "score" : 100
    } */
